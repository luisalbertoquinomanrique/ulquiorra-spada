package VendedorApp;


/**
* VendedorApp/VendedorOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from RegistroVendedor.idl
* mi�rcoles 22 de mayo de 2019 06:27:43 PM COT
*/

public interface VendedorOperations 
{
  boolean insertarVendedor (int cedula, String nombre, String correo, int telefono, String fecha);
  String consultarVendedor (String nombre);
  boolean actualizarVendedor (int idRegistroVendedor, int cedula, String nombre, String correo, int telefono, String fecha);
  boolean eliminarVendedor (int cedula);
  void shutdown ();
} // interface VendedorOperations
