package clases;
import java.sql.*;
import Conexion.Conexion;
import javax.swing.JOptionPane;
import org.omg.CORBA.ORB;

public class Ventas extends VentasApp.VentasPOA
 {
    private ORB orb;
    Conexion conectar = new Conexion();

    @Override
    public boolean insertarVentas(int idVenta, int tipoVenta, int Vendedor, int idProducto, int cantidad, int precioUnitario, String nombre, String fecha) {
boolean resultado = false;
         try{
               String sentenciaSql = "insert into registroVenta (idVenta, fk_Id_Venta, fk_Vendedor, fk_Producto, cantidad, precioUnitario,fecha)"
                         + "values("+ idVenta +","+ tipoVenta +","+ Vendedor +","+ idProducto +","+ cantidad +","+ precioUnitario +",'"+fecha +"')";
               conectar.con();
               Statement st = conectar.conex.createStatement(); //statement ya es elgo pr defecto que trae mysql
               int valor = st.executeUpdate(sentenciaSql); //ejecuteme una actualizacion 
                if(valor > 0){ //siempre me va adevolver un 1 si devuelve un 0 es por que hay un error
                    resultado = true; 
                }
                //Se cierran los recursos
                st.close(); 
                conectar.conex.close();
                
         }catch (Exception e) { 
              JOptionPane.showMessageDialog(null, "Ocurrio un error al insertar una nueva persona. " + e.getMessage());
              
         }
         
         return resultado;        
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String consultarTipoVenta(int tipoVenta) {
        String lista = "";
        try{
             String sentenciSql = "Select * from registroVenta where tipoVenta = " + tipoVenta; 
             conectar.con();             
             Statement st = conectar.conex.createStatement();             
             ResultSet rs = st.executeQuery(sentenciSql);
             while(rs.next()){
                  lista += rs.getLong(2) + " - "
                   + rs.getString(3)+ " - " 
                   + rs.getString(4)+ " - "
                   + rs.getString(5)+ " - " 
                   + rs.getString(6)+ " - "
                   + rs.getString(7)+ " - "
                   + rs.getString(8);
                          }
             //Se cierran los recursos. 
             rs.close();             
             conectar.conex.close();
        }catch(Exception ex){ 
             JOptionPane.showMessageDialog(null, "Ocurrio un error en el catch: " + ex.getMessage());
        }
        return lista;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
   

    @Override
    public String consultarNombre(String nombre) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizarVentas(int tipoVenta, int Vendedor, int idProducto, int cantidad, int precioUnitario, String nombre, String fecha) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminarVentas(String usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String listarVentas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void shutdown() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
     public ResultSet cargarTablaVentas() {
        
        ResultSet resultado = null;         
        try {String query = "select idVenta as Tipo_Venta,registrovendedor.cedula as Id_Vendedor, producto.nombre as producto, cantidad, producto.valorUnitario, registrovendedor.nombre from registrovendedor, producto, registroventa, tipo_venta \n" +
            "where fk_Id_Venta=tipo_venta.Id_Venta\n" +
            "and fk_Vendedor=registrovendedor.idRegistroVendedor \n" +
            "and fk_Producto=producto.idProducto";
        /*select idVenta as Tipo_Venta,registrovendedor.cedula as Id_Vendedor, producto.nombre as producto, cantidad, producto.valorUnitario, registrovendedor.nombreCompleto from registrovendedor, producto, registroventa, tipo_venta 
        where fk_Id_Venta=tipo_venta.Id_Venta
        and fk_Vendedor=registrovendedor.idRegistroVendedor 
        and fk_Producto=producto.idProducto*/
        conectar.con();
        Statement st = conectar.conex.createStatement();
        resultado = st.executeQuery(query);
        
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ocurrio un error: "+e.getMessage());         
        }
        
        return resultado;     
    } 
    
    
    public ResultSet cargarProductos() {
        
        ResultSet resultado = null;         
        try {String query = "SELECT idProducto, nombre FROM `producto`";
        conectar.con();
        Statement st = conectar.conex.createStatement();
        resultado = st.executeQuery(query);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ocurrio un error: "+e.getMessage());         
        }
        return resultado;     
    } 
    
    
    public ResultSet tipoVenta() {
        
        ResultSet resultado = null;         
        try {String query = "SELECT id_Venta, Descripcion FROM `tipo_Venta`";
        conectar.con();
        Statement st = conectar.conex.createStatement();
        resultado = st.executeQuery(query);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ocurrio un error: "+e.getMessage());         
        }
        return resultado;     
    }
    
    public ResultSet Vendedor() {
        
        ResultSet resultado = null;         
        try {String query = "SELECT idRegistroVendedor, nombre FROM `registroVendedor`";
        conectar.con();
        Statement st = conectar.conex.createStatement();
        resultado = st.executeQuery(query);
        
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ocurrio un error: "+e.getMessage());         
        }
        return resultado;     
    }
    
    
    public int NumeroV() {
        
        ResultSet mientras;         
        int resultado = 0;
        try {String query = "SELECT MAX(idVenta) AS contador FROM registroventa";
        conectar.con();
        Statement st = conectar.conex.createStatement();
        mientras = st.executeQuery(query);
        if(mientras.next()){
        resultado = mientras.getInt("contador");
         }
        
            System.out.println(resultado);
            st.close(); 
            conectar.conex.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ocurrio un error: "+e.getMessage());         
        }
        return resultado;     
    }
 

}