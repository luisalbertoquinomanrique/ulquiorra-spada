
package comboboxes;

public class VendedoresCombo {
    private int idRegistroVendedor;
    private String nombreCompleto;
    public VendedoresCombo(int idRegistroVendedor, String nombreCompleto){
        
        this.setIdRegistroVendedor(idRegistroVendedor);
        this.setNombreCompleto(nombreCompleto);
        
    }

    public int getIdRegistroVendedor() {
        return idRegistroVendedor;
    }

    public void setIdRegistroVendedor(int idRegistroVendedor) {
        this.idRegistroVendedor = idRegistroVendedor;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }
    public String toString(){
        return nombreCompleto;
    }
    
    
}
