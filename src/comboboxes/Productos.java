
package comboboxes;


public class Productos {
    private int idProducto;
    private String nombre;
    public Productos(int idProducto, String nombre){
        
        this.setIdProducto(idProducto);
        this.setNombre(nombre);
        
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String toString(){
        return nombre;
    }

    
    
    
    
}
