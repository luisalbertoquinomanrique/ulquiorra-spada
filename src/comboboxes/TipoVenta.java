
package comboboxes;


public class TipoVenta {
    private int Id_Venta;
    private String Descripcion;
    public TipoVenta(int Id_Venta, String Descripcion){
        
        this.setId_Venta(Id_Venta);
        this.setDescripcion(Descripcion);
    }

    public int getId_Venta() {
        return Id_Venta;
    }

    public void setId_Venta(int Id_Venta) {
        this.Id_Venta = Id_Venta;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }
      public String toString(){
        return Descripcion;
    }

    
    


    
    
    
}
