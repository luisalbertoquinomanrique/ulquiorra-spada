-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-05-2019 a las 05:46:59
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectoventas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_Cliente` int(10) NOT NULL,
  `Cedula` int(10) NOT NULL,
  `Nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `Apellido` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_Cliente`, `Cedula`, `Nombre`, `Apellido`) VALUES
(1, 1075305271, 'Luis Alberto', 'Quino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` int(12) NOT NULL,
  `nombre` varchar(40) COLLATE utf8_spanish2_ci NOT NULL,
  `valorUnitario` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idProducto`, `nombre`, `valorUnitario`) VALUES
(0, 'default', 0),
(1, '--Seleccione--', 0),
(2, 'carne', 3500),
(3, 'huevos', 300),
(4, 'pan', 1000),
(5, 'lechuga', 800),
(6, 'papa', 500),
(7, 'salchicha', 5000),
(8, 'yogurt', 2500),
(9, 'atun', 3000),
(10, 'repollo', 700);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registroproveedor`
--

CREATE TABLE `registroproveedor` (
  `idRegistroProveedor` int(15) NOT NULL,
  `cedula` bigint(15) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(70) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `estado` char(1) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `registroproveedor`
--

INSERT INTO `registroproveedor` (`idRegistroProveedor`, `cedula`, `nombre`, `correo`, `telefono`, `estado`, `fecha`) VALUES
(1, 175365296, 'Nicolas Andres Socorro', 'nicol-ande@hotmail.com', 3006585236, 'A', '2018-09-04'),
(2, 1075654987, 'Nelson Paredes', 'pared.nel@hotmail.com', 3136598412, 'A', '2018-01-23'),
(3, 1075369852, 'Juan Pablo Escobar', 'pablojara@hotmail.com', 3059874563, 'A', '2018-03-13'),
(4, 1075589632, 'Pablo Emilio Escobar', 'bandidodebandidos@hotmail.com', 3005894123, 'A', '2018-11-15'),
(5, 1075325741, 'Jhon Jairo Velasquez', 'arrepentido@hotmail.com', 3216985412, 'A', '2018-12-24'),
(6, 1075259753, 'Rodrigo Andres Roa', 'roajara@hotmail.com', 3002589634, 'A', '2018-10-12'),
(7, 1075853453, 'Karla Bahamon', 'lunabella@hotmail.com', 3137845123, 'A', '2018-05-05'),
(8, 1075365563, 'Daniel Olaya', 'oso21@hotmail.com', 3008965231, 'A', '2018-08-08'),
(9, 1075489965, 'Nicolas Yanguma', 'nicoyangu@hotmail.com', 3134959165, 'A', '2018-02-10'),
(10, 1075645445, 'Albeiro Castro', 'albert@hotmail.com', 3002887445, 'A', '2018-06-16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registrousuario`
--

CREATE TABLE `registrousuario` (
  `idRegistroUsuario` int(6) NOT NULL,
  `usuario` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasenia` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `privilegio` char(1) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `registrousuario`
--

INSERT INTO `registrousuario` (`idRegistroUsuario`, `usuario`, `contrasenia`, `privilegio`) VALUES
(1, 'vendedor', '67890', 'V'),
(2, 'admin', '12345', 'A'),
(5, 'KevinAlex', '123456', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registrovendedor`
--

CREATE TABLE `registrovendedor` (
  `idRegistroVendedor` int(50) NOT NULL,
  `cedula` bigint(20) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(70) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` int(15) NOT NULL,
  `estado` char(1) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `registrovendedor`
--

INSERT INTO `registrovendedor` (`idRegistroVendedor`, `cedula`, `nombre`, `correo`, `telefono`, `estado`, `fecha`) VALUES
(0, 0, '--Seleccione--', 'defoult@hotmail.com', 0, 'A', '2018-05-12'),
(1, 12135013, 'Prueba', 'p´rueba@hotmail.com', 0, 'A', '2018-05-12'),
(2, 1075896541, 'Luis 1', 'arcesebi@hotmail.com', 0, 'A', '2018-06-21'),
(3, 1075359963, 'Luis 2', 'cuellin@hotmail.com', 0, 'A', '2018-05-04'),
(4, 1075853369, 'Luis 3', 'esan@hotmail.com', 0, 'A', '2018-07-07'),
(5, 1075896325, 'Luis 4', 'santiman@hotmail.com', 0, 'A', '2018-02-08'),
(6, 1075936456, 'Luis 5', 'anita@hotmail.com', 0, 'A', '2018-12-05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registroventa`
--

CREATE TABLE `registroventa` (
  `idVenta` int(11) NOT NULL,
  `fk_Id_Venta` int(10) NOT NULL,
  `fk_Vendedor` int(50) NOT NULL,
  `fk_Producto` int(12) NOT NULL,
  `cantidad` int(20) NOT NULL,
  `precioUnitario` decimal(10,0) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `registroventa`
--

INSERT INTO `registroventa` (`idVenta`, `fk_Id_Venta`, `fk_Vendedor`, `fk_Producto`, `cantidad`, `precioUnitario`, `fecha`) VALUES
(1, 1, 1, 2, 8, '3500', '2019-05-22'),
(1, 2, 2, 6, 8, '500', '2019-05-22'),
(2, 1, 2, 2, 9, '3500', '2019-05-22'),
(2, 2, 2, 4, 9, '1000', '2019-05-22'),
(2, 2, 2, 5, 9, '800', '2019-05-22'),
(2, 2, 2, 7, 9, '5000', '2019-05-22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_venta`
--

CREATE TABLE `tipo_venta` (
  `Id_Venta` int(10) NOT NULL,
  `Descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `tipo_venta`
--

INSERT INTO `tipo_venta` (`Id_Venta`, `Descripcion`) VALUES
(0, '--Seleccione--'),
(1, 'Contado'),
(2, 'Credito'),
(3, 'Bitcoin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_Cliente`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `registroproveedor`
--
ALTER TABLE `registroproveedor`
  ADD PRIMARY KEY (`idRegistroProveedor`);

--
-- Indices de la tabla `registrousuario`
--
ALTER TABLE `registrousuario`
  ADD PRIMARY KEY (`idRegistroUsuario`);

--
-- Indices de la tabla `registrovendedor`
--
ALTER TABLE `registrovendedor`
  ADD PRIMARY KEY (`idRegistroVendedor`);

--
-- Indices de la tabla `registroventa`
--
ALTER TABLE `registroventa`
  ADD PRIMARY KEY (`idVenta`,`fk_Producto`),
  ADD KEY `fk_registrodevetnas_producto` (`fk_Producto`) USING BTREE,
  ADD KEY `fk_Id_Venta` (`fk_Id_Venta`),
  ADD KEY `fk_Vendedor` (`fk_Vendedor`);

--
-- Indices de la tabla `tipo_venta`
--
ALTER TABLE `tipo_venta`
  ADD PRIMARY KEY (`Id_Venta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_Cliente` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `registrousuario`
--
ALTER TABLE `registrousuario`
  MODIFY `idRegistroUsuario` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `registroventa`
--
ALTER TABLE `registroventa`
  MODIFY `idVenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `registroventa`
--
ALTER TABLE `registroventa`
  ADD CONSTRAINT `fk_registrodevetnas_producto` FOREIGN KEY (`fk_Producto`) REFERENCES `producto` (`idProducto`) ON DELETE NO ACTION,
  ADD CONSTRAINT `fk_registroventas_tipoVenta` FOREIGN KEY (`fk_Id_Venta`) REFERENCES `tipo_venta` (`Id_Venta`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
