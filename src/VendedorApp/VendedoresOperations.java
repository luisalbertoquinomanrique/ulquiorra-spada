package VendedorApp;


/**
* VendedorApp/VendedoresOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from MenuVendedor.idl
* martes 21 de mayo de 2019 01:48:39 AM COT
*/

public interface VendedoresOperations 
{
  boolean insertarVendedor (int cedula, String nombreCompleto, String correo, String estado, String fecha);
  String consultarVendedorNombre (String nombreCompleto);
  String consultarVendedorFecha (String fecha);
  String consultarVendedorcedula (int cedula);
  boolean actualizarVendedor (int cedula, String nombreCompleto, String correo, String estado, String fecha);
  boolean eliminarVendedorNombre (String nombreCompleto);
  boolean eliminarVendedorCedula (int cedula);
  void shutdown ();
} // interface VendedoresOperations
